# Regex

## Snippets

From the start of the line, select numbers which are in sequences of 10 to 13 in length and terminate the line.

Select sequences of digits 10-13 numbers in length which constitute everything on the line on which they appear.

```
^(\d{10,13})$
```

Note: this also puts those numbers in a capture group.
