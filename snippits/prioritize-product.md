

```sql
-- update feed_merge.PUBLISHINGQUEUE pq
-- set pq.PRIORITY = 5
select * from feed_merge.PUBLISHINGQUEUE pq
where itemtype = 'FeedAlbum'
  and status not in (3)
      and ITEMID in (
select fa.feedalbumid from feed_merge.album fa
join indie.ALBUM ua on to_char(ua.albumid) = fa.PROVIDERITEMID and fa.provider = 'ORC'
and ua.upc in (
                 '00044003274549',
                 '00044003274990'
))
